require 'test_helper'

class AsignaturaAlumnosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @asignatura_alumno = asignatura_alumnos(:one)
  end

  test "should get index" do
    get asignatura_alumnos_url
    assert_response :success
  end

  test "should get new" do
    get new_asignatura_alumno_url
    assert_response :success
  end

  test "should create asignatura_alumno" do
    assert_difference('AsignaturaAlumno.count') do
      post asignatura_alumnos_url, params: { asignatura_alumno: { alumno_id: @asignatura_alumno.alumno_id, asignatura_id: @asignatura_alumno.asignatura_id, nota: @asignatura_alumno.nota, ponderacion: @asignatura_alumno.ponderacion } }
    end

    assert_redirected_to asignatura_alumno_url(AsignaturaAlumno.last)
  end

  test "should show asignatura_alumno" do
    get asignatura_alumno_url(@asignatura_alumno)
    assert_response :success
  end

  test "should get edit" do
    get edit_asignatura_alumno_url(@asignatura_alumno)
    assert_response :success
  end

  test "should update asignatura_alumno" do
    patch asignatura_alumno_url(@asignatura_alumno), params: { asignatura_alumno: { alumno_id: @asignatura_alumno.alumno_id, asignatura_id: @asignatura_alumno.asignatura_id, nota: @asignatura_alumno.nota, ponderacion: @asignatura_alumno.ponderacion } }
    assert_redirected_to asignatura_alumno_url(@asignatura_alumno)
  end

  test "should destroy asignatura_alumno" do
    assert_difference('AsignaturaAlumno.count', -1) do
      delete asignatura_alumno_url(@asignatura_alumno)
    end

    assert_redirected_to asignatura_alumnos_url
  end
end
