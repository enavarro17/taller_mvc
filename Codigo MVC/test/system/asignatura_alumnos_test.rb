require "application_system_test_case"

class AsignaturaAlumnosTest < ApplicationSystemTestCase
  setup do
    @asignatura_alumno = asignatura_alumnos(:one)
  end

  test "visiting the index" do
    visit asignatura_alumnos_url
    assert_selector "h1", text: "Asignatura Alumnos"
  end

  test "creating a Asignatura alumno" do
    visit asignatura_alumnos_url
    click_on "New Asignatura Alumno"

    fill_in "Alumno", with: @asignatura_alumno.alumno_id
    fill_in "Asignatura", with: @asignatura_alumno.asignatura_id
    fill_in "Nota", with: @asignatura_alumno.nota
    fill_in "Ponderacion", with: @asignatura_alumno.ponderacion
    click_on "Create Asignatura alumno"

    assert_text "Asignatura alumno was successfully created"
    click_on "Back"
  end

  test "updating a Asignatura alumno" do
    visit asignatura_alumnos_url
    click_on "Edit", match: :first

    fill_in "Alumno", with: @asignatura_alumno.alumno_id
    fill_in "Asignatura", with: @asignatura_alumno.asignatura_id
    fill_in "Nota", with: @asignatura_alumno.nota
    fill_in "Ponderacion", with: @asignatura_alumno.ponderacion
    click_on "Update Asignatura alumno"

    assert_text "Asignatura alumno was successfully updated"
    click_on "Back"
  end

  test "destroying a Asignatura alumno" do
    visit asignatura_alumnos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Asignatura alumno was successfully destroyed"
  end
end
