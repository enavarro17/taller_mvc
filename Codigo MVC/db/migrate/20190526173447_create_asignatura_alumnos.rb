class CreateAsignaturaAlumnos < ActiveRecord::Migration[5.2]
  def change
    create_table :asignatura_alumnos do |t|
      t.integer :nota
      t.integer :ponderacion
      t.references :asignatura, foreign_key: true
      t.references :alumno, foreign_key: true

      t.timestamps
    end
  end
end
