class CreateAulas < ActiveRecord::Migration[5.2]
  def change
    create_table :aulas do |t|
      t.integer :numero
      t.string :nombre
      t.references :horario, foreign_key: true

      t.timestamps
    end
  end
end
