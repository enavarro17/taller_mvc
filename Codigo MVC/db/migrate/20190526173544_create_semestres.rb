class CreateSemestres < ActiveRecord::Migration[5.2]
  def change
    create_table :semestres do |t|
      t.integer :periodo_academico
      t.references :asignatura, foreign_key: true

      t.timestamps
    end
  end
end
