class CreateFacultads < ActiveRecord::Migration[5.2]
  def change
    create_table :facultads do |t|
      t.string :nombre
      t.string :ubicacion
      t.integer :telefono
      t.string :director
      t.references :carrera, foreign_key: true

      t.timestamps
    end
  end
end
