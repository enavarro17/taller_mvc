class CreateProfesors < ActiveRecord::Migration[5.2]
  def change
    create_table :profesors do |t|
      t.string :titulo
      t.references :usuario, foreign_key: true

      t.timestamps
    end
  end
end
