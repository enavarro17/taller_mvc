# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_26_173807) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alumnos", force: :cascade do |t|
    t.bigint "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["usuario_id"], name: "index_alumnos_on_usuario_id"
  end

  create_table "asignatura_alumnos", force: :cascade do |t|
    t.integer "nota"
    t.integer "ponderacion"
    t.bigint "asignatura_id"
    t.bigint "alumno_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["alumno_id"], name: "index_asignatura_alumnos_on_alumno_id"
    t.index ["asignatura_id"], name: "index_asignatura_alumnos_on_asignatura_id"
  end

  create_table "asignaturas", force: :cascade do |t|
    t.string "nombre"
    t.integer "credito"
    t.integer "seccion"
    t.integer "nrc"
    t.bigint "profesor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["profesor_id"], name: "index_asignaturas_on_profesor_id"
  end

  create_table "aulas", force: :cascade do |t|
    t.integer "numero"
    t.string "nombre"
    t.bigint "horario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["horario_id"], name: "index_aulas_on_horario_id"
  end

  create_table "carreras", force: :cascade do |t|
    t.string "nombre"
    t.string "campus"
    t.bigint "semestre_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["semestre_id"], name: "index_carreras_on_semestre_id"
  end

  create_table "facultads", force: :cascade do |t|
    t.string "nombre"
    t.string "ubicacion"
    t.integer "telefono"
    t.string "director"
    t.bigint "carrera_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["carrera_id"], name: "index_facultads_on_carrera_id"
  end

  create_table "horarios", force: :cascade do |t|
    t.integer "bloque"
    t.bigint "asignatura_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["asignatura_id"], name: "index_horarios_on_asignatura_id"
  end

  create_table "profesors", force: :cascade do |t|
    t.string "titulo"
    t.bigint "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["usuario_id"], name: "index_profesors_on_usuario_id"
  end

  create_table "semestres", force: :cascade do |t|
    t.integer "periodo_academico"
    t.bigint "asignatura_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["asignatura_id"], name: "index_semestres_on_asignatura_id"
  end

  create_table "usuarios", force: :cascade do |t|
    t.string "nombre"
    t.string "apellido"
    t.string "nombre_usuario"
    t.string "contrasena"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "alumnos", "usuarios"
  add_foreign_key "asignatura_alumnos", "alumnos"
  add_foreign_key "asignatura_alumnos", "asignaturas"
  add_foreign_key "aulas", "horarios"
  add_foreign_key "carreras", "semestres"
  add_foreign_key "facultads", "carreras"
  add_foreign_key "horarios", "asignaturas"
  add_foreign_key "profesors", "usuarios"
  add_foreign_key "semestres", "asignaturas"
end
