json.extract! semestre, :id, :periodo_academico, :asignatura_id, :created_at, :updated_at
json.url semestre_url(semestre, format: :json)
