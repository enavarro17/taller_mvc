json.extract! aula, :id, :numero, :nombre, :horario_id, :created_at, :updated_at
json.url aula_url(aula, format: :json)
