json.extract! asignatura, :id, :nombre, :credito, :seccion, :nrc, :profesor_id, :created_at, :updated_at
json.url asignatura_url(asignatura, format: :json)
