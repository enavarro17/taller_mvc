json.extract! asignatura_alumno, :id, :nota, :ponderacion, :asignatura_id, :alumno_id, :created_at, :updated_at
json.url asignatura_alumno_url(asignatura_alumno, format: :json)
