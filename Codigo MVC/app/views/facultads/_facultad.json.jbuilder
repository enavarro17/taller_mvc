json.extract! facultad, :id, :nombre, :ubicacion, :telefono, :director, :carrera_id, :created_at, :updated_at
json.url facultad_url(facultad, format: :json)
