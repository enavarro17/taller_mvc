json.extract! carrera, :id, :nombre, :campus, :semestre_id, :created_at, :updated_at
json.url carrera_url(carrera, format: :json)
