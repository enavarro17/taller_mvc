json.extract! usuario, :id, :nombre, :apellido, :nombre_usuario, :contrasena, :created_at, :updated_at
json.url usuario_url(usuario, format: :json)
