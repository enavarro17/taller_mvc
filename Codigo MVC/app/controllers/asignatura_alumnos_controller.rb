class AsignaturaAlumnosController < ApplicationController
  before_action :set_asignatura_alumno, only: [:show, :edit, :update, :destroy]

  # GET /asignatura_alumnos
  # GET /asignatura_alumnos.json
  def index
    @asignatura_alumnos = AsignaturaAlumno.all
  end

  # GET /asignatura_alumnos/1
  # GET /asignatura_alumnos/1.json
  def show
  end

  # GET /asignatura_alumnos/new
  def new
    @asignatura_alumno = AsignaturaAlumno.new
  end

  # GET /asignatura_alumnos/1/edit
  def edit
  end

  # POST /asignatura_alumnos
  # POST /asignatura_alumnos.json
  def create
    @asignatura_alumno = AsignaturaAlumno.new(asignatura_alumno_params)

    respond_to do |format|
      if @asignatura_alumno.save
        format.html { redirect_to @asignatura_alumno, notice: 'Asignatura alumno was successfully created.' }
        format.json { render :show, status: :created, location: @asignatura_alumno }
      else
        format.html { render :new }
        format.json { render json: @asignatura_alumno.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /asignatura_alumnos/1
  # PATCH/PUT /asignatura_alumnos/1.json
  def update
    respond_to do |format|
      if @asignatura_alumno.update(asignatura_alumno_params)
        format.html { redirect_to @asignatura_alumno, notice: 'Asignatura alumno was successfully updated.' }
        format.json { render :show, status: :ok, location: @asignatura_alumno }
      else
        format.html { render :edit }
        format.json { render json: @asignatura_alumno.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /asignatura_alumnos/1
  # DELETE /asignatura_alumnos/1.json
  def destroy
    @asignatura_alumno.destroy
    respond_to do |format|
      format.html { redirect_to asignatura_alumnos_url, notice: 'Asignatura alumno was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asignatura_alumno
      @asignatura_alumno = AsignaturaAlumno.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asignatura_alumno_params
      params.require(:asignatura_alumno).permit(:nota, :ponderacion, :asignatura_id, :alumno_id)
    end
end
