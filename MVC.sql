--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

-- Started on 2019-05-26 14:19:48

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 192 (class 1259 OID 25067)
-- Name: alumnos; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.alumnos (
    id bigint NOT NULL,
    usuario_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.alumnos OWNER TO enavarro;

--
-- TOC entry 191 (class 1259 OID 25065)
-- Name: alumnos_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.alumnos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alumnos_id_seq OWNER TO enavarro;

--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 191
-- Name: alumnos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.alumnos_id_seq OWNED BY public.alumnos.id;


--
-- TOC entry 186 (class 1259 OID 25029)
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO enavarro;

--
-- TOC entry 200 (class 1259 OID 25124)
-- Name: asignatura_alumnos; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.asignatura_alumnos (
    id bigint NOT NULL,
    nota integer,
    ponderacion integer,
    asignatura_id bigint,
    alumno_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.asignatura_alumnos OWNER TO enavarro;

--
-- TOC entry 199 (class 1259 OID 25122)
-- Name: asignatura_alumnos_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.asignatura_alumnos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asignatura_alumnos_id_seq OWNER TO enavarro;

--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 199
-- Name: asignatura_alumnos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.asignatura_alumnos_id_seq OWNED BY public.asignatura_alumnos.id;


--
-- TOC entry 194 (class 1259 OID 25081)
-- Name: asignaturas; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.asignaturas (
    id bigint NOT NULL,
    nombre character varying,
    credito integer,
    seccion integer,
    nrc integer,
    profesor_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.asignaturas OWNER TO enavarro;

--
-- TOC entry 193 (class 1259 OID 25079)
-- Name: asignaturas_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.asignaturas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asignaturas_id_seq OWNER TO enavarro;

--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 193
-- Name: asignaturas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.asignaturas_id_seq OWNED BY public.asignaturas.id;


--
-- TOC entry 198 (class 1259 OID 25107)
-- Name: aulas; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.aulas (
    id bigint NOT NULL,
    numero integer,
    nombre character varying,
    horario_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.aulas OWNER TO enavarro;

--
-- TOC entry 197 (class 1259 OID 25105)
-- Name: aulas_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.aulas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aulas_id_seq OWNER TO enavarro;

--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 197
-- Name: aulas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.aulas_id_seq OWNED BY public.aulas.id;


--
-- TOC entry 204 (class 1259 OID 25158)
-- Name: carreras; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.carreras (
    id bigint NOT NULL,
    nombre character varying,
    campus character varying,
    semestre_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.carreras OWNER TO enavarro;

--
-- TOC entry 203 (class 1259 OID 25156)
-- Name: carreras_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.carreras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.carreras_id_seq OWNER TO enavarro;

--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 203
-- Name: carreras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.carreras_id_seq OWNED BY public.carreras.id;


--
-- TOC entry 206 (class 1259 OID 25175)
-- Name: facultads; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.facultads (
    id bigint NOT NULL,
    nombre character varying,
    ubicacion character varying,
    telefono integer,
    director character varying,
    carrera_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.facultads OWNER TO enavarro;

--
-- TOC entry 205 (class 1259 OID 25173)
-- Name: facultads_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.facultads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.facultads_id_seq OWNER TO enavarro;

--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 205
-- Name: facultads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.facultads_id_seq OWNED BY public.facultads.id;


--
-- TOC entry 196 (class 1259 OID 25093)
-- Name: horarios; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.horarios (
    id bigint NOT NULL,
    bloque integer,
    asignatura_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.horarios OWNER TO enavarro;

--
-- TOC entry 195 (class 1259 OID 25091)
-- Name: horarios_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.horarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.horarios_id_seq OWNER TO enavarro;

--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 195
-- Name: horarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.horarios_id_seq OWNED BY public.horarios.id;


--
-- TOC entry 190 (class 1259 OID 25050)
-- Name: profesors; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.profesors (
    id bigint NOT NULL,
    titulo character varying,
    usuario_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.profesors OWNER TO enavarro;

--
-- TOC entry 189 (class 1259 OID 25048)
-- Name: profesors_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.profesors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profesors_id_seq OWNER TO enavarro;

--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 189
-- Name: profesors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.profesors_id_seq OWNED BY public.profesors.id;


--
-- TOC entry 185 (class 1259 OID 25021)
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO enavarro;

--
-- TOC entry 202 (class 1259 OID 25144)
-- Name: semestres; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.semestres (
    id bigint NOT NULL,
    periodo_academico integer,
    asignatura_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.semestres OWNER TO enavarro;

--
-- TOC entry 201 (class 1259 OID 25142)
-- Name: semestres_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.semestres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.semestres_id_seq OWNER TO enavarro;

--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 201
-- Name: semestres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.semestres_id_seq OWNED BY public.semestres.id;


--
-- TOC entry 188 (class 1259 OID 25039)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: enavarro
--

CREATE TABLE public.usuarios (
    id bigint NOT NULL,
    nombre character varying,
    apellido character varying,
    nombre_usuario character varying,
    contrasena character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.usuarios OWNER TO enavarro;

--
-- TOC entry 187 (class 1259 OID 25037)
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: enavarro
--

CREATE SEQUENCE public.usuarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO enavarro;

--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 187
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: enavarro
--

ALTER SEQUENCE public.usuarios_id_seq OWNED BY public.usuarios.id;


--
-- TOC entry 2073 (class 2604 OID 25070)
-- Name: alumnos id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.alumnos ALTER COLUMN id SET DEFAULT nextval('public.alumnos_id_seq'::regclass);


--
-- TOC entry 2077 (class 2604 OID 25127)
-- Name: asignatura_alumnos id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.asignatura_alumnos ALTER COLUMN id SET DEFAULT nextval('public.asignatura_alumnos_id_seq'::regclass);


--
-- TOC entry 2074 (class 2604 OID 25084)
-- Name: asignaturas id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.asignaturas ALTER COLUMN id SET DEFAULT nextval('public.asignaturas_id_seq'::regclass);


--
-- TOC entry 2076 (class 2604 OID 25110)
-- Name: aulas id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.aulas ALTER COLUMN id SET DEFAULT nextval('public.aulas_id_seq'::regclass);


--
-- TOC entry 2079 (class 2604 OID 25161)
-- Name: carreras id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.carreras ALTER COLUMN id SET DEFAULT nextval('public.carreras_id_seq'::regclass);


--
-- TOC entry 2080 (class 2604 OID 25178)
-- Name: facultads id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.facultads ALTER COLUMN id SET DEFAULT nextval('public.facultads_id_seq'::regclass);


--
-- TOC entry 2075 (class 2604 OID 25096)
-- Name: horarios id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.horarios ALTER COLUMN id SET DEFAULT nextval('public.horarios_id_seq'::regclass);


--
-- TOC entry 2072 (class 2604 OID 25053)
-- Name: profesors id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.profesors ALTER COLUMN id SET DEFAULT nextval('public.profesors_id_seq'::regclass);


--
-- TOC entry 2078 (class 2604 OID 25147)
-- Name: semestres id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.semestres ALTER COLUMN id SET DEFAULT nextval('public.semestres_id_seq'::regclass);


--
-- TOC entry 2071 (class 2604 OID 25042)
-- Name: usuarios id; Type: DEFAULT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuarios_id_seq'::regclass);


--
-- TOC entry 2248 (class 0 OID 25067)
-- Dependencies: 192
-- Data for Name: alumnos; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.alumnos (id, usuario_id, created_at, updated_at) FROM stdin;
1	1	2019-05-26 17:50:17.512781	2019-05-26 17:50:17.512781
2	2	2019-05-26 17:50:23.237556	2019-05-26 17:50:23.237556
\.


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 191
-- Name: alumnos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.alumnos_id_seq', 2, true);


--
-- TOC entry 2242 (class 0 OID 25029)
-- Dependencies: 186
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2019-05-26 17:39:59.014166	2019-05-26 17:39:59.014166
\.


--
-- TOC entry 2256 (class 0 OID 25124)
-- Dependencies: 200
-- Data for Name: asignatura_alumnos; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.asignatura_alumnos (id, nota, ponderacion, asignatura_id, alumno_id, created_at, updated_at) FROM stdin;
1	70	100	1	1	2019-05-26 18:17:53.661237	2019-05-26 18:17:53.661237
2	70	100	2	1	2019-05-26 18:18:02.278288	2019-05-26 18:18:02.278288
3	70	100	3	1	2019-05-26 18:18:12.858943	2019-05-26 18:18:12.858943
4	50	100	1	2	2019-05-26 18:18:26.819521	2019-05-26 18:18:26.819521
5	30	100	2	2	2019-05-26 18:18:37.797093	2019-05-26 18:18:37.797093
\.


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 199
-- Name: asignatura_alumnos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.asignatura_alumnos_id_seq', 5, true);


--
-- TOC entry 2250 (class 0 OID 25081)
-- Dependencies: 194
-- Data for Name: asignaturas; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.asignaturas (id, nombre, credito, seccion, nrc, profesor_id, created_at, updated_at) FROM stdin;
1	Programacion	1	601	1111	4	2019-05-26 17:45:42.414643	2019-05-26 17:45:42.414643
2	Arquitectura	2	601	2222	3	2019-05-26 17:46:00.517867	2019-05-26 17:46:00.517867
5	Introduccion	1	601	5555	3	2019-05-26 17:48:51.450134	2019-05-26 17:48:51.450134
3	Infraestructura	1	601	3333	3	2019-05-26 17:47:47.285209	2019-05-26 18:01:47.338484
4	Redes	1	601	4444	3	2019-05-26 17:48:29.289267	2019-05-26 18:01:53.183533
\.


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 193
-- Name: asignaturas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.asignaturas_id_seq', 5, true);


--
-- TOC entry 2254 (class 0 OID 25107)
-- Dependencies: 198
-- Data for Name: aulas; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.aulas (id, numero, nombre, horario_id, created_at, updated_at) FROM stdin;
1	601	SALA601	1	2019-05-26 17:59:37.524019	2019-05-26 17:59:37.524019
2	602	SALA602	2	2019-05-26 17:59:52.898561	2019-05-26 17:59:52.898561
3	603	SALA603	3	2019-05-26 18:00:03.458939	2019-05-26 18:00:03.458939
4	604	SALA604	4	2019-05-26 18:00:12.821733	2019-05-26 18:00:12.821733
5	605	SALA605	5	2019-05-26 18:00:20.763557	2019-05-26 18:00:20.763557
\.


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 197
-- Name: aulas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.aulas_id_seq', 5, true);


--
-- TOC entry 2260 (class 0 OID 25158)
-- Dependencies: 204
-- Data for Name: carreras; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.carreras (id, nombre, campus, semestre_id, created_at, updated_at) FROM stdin;
1	Informatica	VM	1	2019-05-26 17:57:38.281293	2019-05-26 17:57:38.281293
2	Derecho	VM	1	2019-05-26 17:57:54.114654	2019-05-26 17:57:54.114654
3	Psicologia	VM	1	2019-05-26 17:58:03.383731	2019-05-26 17:58:03.383731
4	Ingles	VM	1	2019-05-26 17:58:19.29451	2019-05-26 17:58:19.29451
5	Odontologia	VM	1	2019-05-26 17:58:30.887724	2019-05-26 17:58:30.887724
\.


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 203
-- Name: carreras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.carreras_id_seq', 5, true);


--
-- TOC entry 2262 (class 0 OID 25175)
-- Dependencies: 206
-- Data for Name: facultads; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.facultads (id, nombre, ubicacion, telefono, director, carrera_id, created_at, updated_at) FROM stdin;
1	Facultad Ingenieria	Segundo Piso	1111111111	Romina	1	2019-05-26 18:03:16.445341	2019-05-26 18:03:16.445341
2	Derecho	Sexto Piso	11111111	Juanito Perez	2	2019-05-26 18:03:35.70906	2019-05-26 18:03:35.70906
\.


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 205
-- Name: facultads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.facultads_id_seq', 2, true);


--
-- TOC entry 2252 (class 0 OID 25093)
-- Dependencies: 196
-- Data for Name: horarios; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.horarios (id, bloque, asignatura_id, created_at, updated_at) FROM stdin;
1	1	1	2019-05-26 17:53:50.560304	2019-05-26 17:53:50.560304
2	2	2	2019-05-26 17:53:58.92513	2019-05-26 17:53:58.92513
3	10	3	2019-05-26 17:54:09.039788	2019-05-26 17:54:09.039788
4	8	4	2019-05-26 17:54:16.388709	2019-05-26 17:54:16.388709
5	2	5	2019-05-26 17:54:23.321818	2019-05-26 17:54:23.321818
\.


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 195
-- Name: horarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.horarios_id_seq', 5, true);


--
-- TOC entry 2246 (class 0 OID 25050)
-- Dependencies: 190
-- Data for Name: profesors; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.profesors (id, titulo, usuario_id, created_at, updated_at) FROM stdin;
1	Informatico	3	2019-05-26 17:49:35.317863	2019-05-26 17:49:35.317863
2	Informatico	4	2019-05-26 17:49:50.088659	2019-05-26 17:49:50.088659
3	Informatico	5	2019-05-26 17:49:55.75731	2019-05-26 17:49:55.75731
4	Informatico	6	2019-05-26 17:50:01.094801	2019-05-26 17:50:01.094801
\.


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 189
-- Name: profesors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.profesors_id_seq', 4, true);


--
-- TOC entry 2241 (class 0 OID 25021)
-- Dependencies: 185
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.schema_migrations (version) FROM stdin;
20190526164957
20190526170108
20190526170243
20190526171158
20190526173218
20190526173306
20190526173447
20190526173544
20190526173715
20190526173807
\.


--
-- TOC entry 2258 (class 0 OID 25144)
-- Dependencies: 202
-- Data for Name: semestres; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.semestres (id, periodo_academico, asignatura_id, created_at, updated_at) FROM stdin;
1	201901	1	2019-05-26 17:55:46.930364	2019-05-26 17:55:46.930364
2	201901	2	2019-05-26 17:55:53.218186	2019-05-26 17:55:53.218186
3	201901	3	2019-05-26 17:56:00.83078	2019-05-26 17:56:00.83078
4	201901	4	2019-05-26 17:56:06.679383	2019-05-26 17:56:06.679383
5	201901	5	2019-05-26 17:56:12.881413	2019-05-26 17:56:12.881413
\.


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 201
-- Name: semestres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.semestres_id_seq', 5, true);


--
-- TOC entry 2244 (class 0 OID 25039)
-- Dependencies: 188
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: enavarro
--

COPY public.usuarios (id, nombre, apellido, nombre_usuario, contrasena, created_at, updated_at) FROM stdin;
1	Eduardo	Navarro	enavarro	unab2019	2019-05-26 17:42:10.054813	2019-05-26 17:42:10.054813
2	Cristian	Gamboa	cgamboa	unab2019	2019-05-26 17:44:54.192733	2019-05-26 17:44:54.192733
3	Romina	Torres	rtorres	unab2019	2019-05-26 17:46:53.552652	2019-05-26 17:46:53.552652
4	Juan	Calderon	juan.calderon	unab2019	2019-05-26 17:47:05.028926	2019-05-26 17:47:05.028926
5	Karlo	Laguans	klagunas	unab2019	2019-05-26 17:47:16.117321	2019-05-26 17:47:16.117321
6	Omar	Salinas	osalinas	unab2019	2019-05-26 17:48:00.134972	2019-05-26 17:48:00.134972
\.


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 187
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: enavarro
--

SELECT pg_catalog.setval('public.usuarios_id_seq', 6, true);


--
-- TOC entry 2091 (class 2606 OID 25072)
-- Name: alumnos alumnos_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.alumnos
    ADD CONSTRAINT alumnos_pkey PRIMARY KEY (id);


--
-- TOC entry 2084 (class 2606 OID 25036)
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- TOC entry 2103 (class 2606 OID 25129)
-- Name: asignatura_alumnos asignatura_alumnos_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.asignatura_alumnos
    ADD CONSTRAINT asignatura_alumnos_pkey PRIMARY KEY (id);


--
-- TOC entry 2094 (class 2606 OID 25089)
-- Name: asignaturas asignaturas_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.asignaturas
    ADD CONSTRAINT asignaturas_pkey PRIMARY KEY (id);


--
-- TOC entry 2100 (class 2606 OID 25115)
-- Name: aulas aulas_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.aulas
    ADD CONSTRAINT aulas_pkey PRIMARY KEY (id);


--
-- TOC entry 2110 (class 2606 OID 25166)
-- Name: carreras carreras_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.carreras
    ADD CONSTRAINT carreras_pkey PRIMARY KEY (id);


--
-- TOC entry 2113 (class 2606 OID 25183)
-- Name: facultads facultads_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.facultads
    ADD CONSTRAINT facultads_pkey PRIMARY KEY (id);


--
-- TOC entry 2097 (class 2606 OID 25098)
-- Name: horarios horarios_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.horarios
    ADD CONSTRAINT horarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2089 (class 2606 OID 25058)
-- Name: profesors profesors_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.profesors
    ADD CONSTRAINT profesors_pkey PRIMARY KEY (id);


--
-- TOC entry 2082 (class 2606 OID 25028)
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- TOC entry 2108 (class 2606 OID 25149)
-- Name: semestres semestres_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.semestres
    ADD CONSTRAINT semestres_pkey PRIMARY KEY (id);


--
-- TOC entry 2086 (class 2606 OID 25047)
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2092 (class 1259 OID 25078)
-- Name: index_alumnos_on_usuario_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_alumnos_on_usuario_id ON public.alumnos USING btree (usuario_id);


--
-- TOC entry 2104 (class 1259 OID 25141)
-- Name: index_asignatura_alumnos_on_alumno_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_asignatura_alumnos_on_alumno_id ON public.asignatura_alumnos USING btree (alumno_id);


--
-- TOC entry 2105 (class 1259 OID 25140)
-- Name: index_asignatura_alumnos_on_asignatura_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_asignatura_alumnos_on_asignatura_id ON public.asignatura_alumnos USING btree (asignatura_id);


--
-- TOC entry 2095 (class 1259 OID 25090)
-- Name: index_asignaturas_on_profesor_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_asignaturas_on_profesor_id ON public.asignaturas USING btree (profesor_id);


--
-- TOC entry 2101 (class 1259 OID 25121)
-- Name: index_aulas_on_horario_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_aulas_on_horario_id ON public.aulas USING btree (horario_id);


--
-- TOC entry 2111 (class 1259 OID 25172)
-- Name: index_carreras_on_semestre_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_carreras_on_semestre_id ON public.carreras USING btree (semestre_id);


--
-- TOC entry 2114 (class 1259 OID 25189)
-- Name: index_facultads_on_carrera_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_facultads_on_carrera_id ON public.facultads USING btree (carrera_id);


--
-- TOC entry 2098 (class 1259 OID 25104)
-- Name: index_horarios_on_asignatura_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_horarios_on_asignatura_id ON public.horarios USING btree (asignatura_id);


--
-- TOC entry 2087 (class 1259 OID 25064)
-- Name: index_profesors_on_usuario_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_profesors_on_usuario_id ON public.profesors USING btree (usuario_id);


--
-- TOC entry 2106 (class 1259 OID 25155)
-- Name: index_semestres_on_asignatura_id; Type: INDEX; Schema: public; Owner: enavarro
--

CREATE INDEX index_semestres_on_asignatura_id ON public.semestres USING btree (asignatura_id);


--
-- TOC entry 2123 (class 2606 OID 25184)
-- Name: facultads fk_rails_1fac1696b7; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.facultads
    ADD CONSTRAINT fk_rails_1fac1696b7 FOREIGN KEY (carrera_id) REFERENCES public.carreras(id);


--
-- TOC entry 2118 (class 2606 OID 25116)
-- Name: aulas fk_rails_386ffe74f4; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.aulas
    ADD CONSTRAINT fk_rails_386ffe74f4 FOREIGN KEY (horario_id) REFERENCES public.horarios(id);


--
-- TOC entry 2120 (class 2606 OID 25135)
-- Name: asignatura_alumnos fk_rails_65a978dfe7; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.asignatura_alumnos
    ADD CONSTRAINT fk_rails_65a978dfe7 FOREIGN KEY (alumno_id) REFERENCES public.alumnos(id);


--
-- TOC entry 2116 (class 2606 OID 25073)
-- Name: alumnos fk_rails_7b573e5907; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.alumnos
    ADD CONSTRAINT fk_rails_7b573e5907 FOREIGN KEY (usuario_id) REFERENCES public.usuarios(id);


--
-- TOC entry 2119 (class 2606 OID 25130)
-- Name: asignatura_alumnos fk_rails_9178c7ec7e; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.asignatura_alumnos
    ADD CONSTRAINT fk_rails_9178c7ec7e FOREIGN KEY (asignatura_id) REFERENCES public.asignaturas(id);


--
-- TOC entry 2121 (class 2606 OID 25150)
-- Name: semestres fk_rails_95d0267059; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.semestres
    ADD CONSTRAINT fk_rails_95d0267059 FOREIGN KEY (asignatura_id) REFERENCES public.asignaturas(id);


--
-- TOC entry 2117 (class 2606 OID 25099)
-- Name: horarios fk_rails_be4f39e94b; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.horarios
    ADD CONSTRAINT fk_rails_be4f39e94b FOREIGN KEY (asignatura_id) REFERENCES public.asignaturas(id);


--
-- TOC entry 2115 (class 2606 OID 25059)
-- Name: profesors fk_rails_d402d36b11; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.profesors
    ADD CONSTRAINT fk_rails_d402d36b11 FOREIGN KEY (usuario_id) REFERENCES public.usuarios(id);


--
-- TOC entry 2122 (class 2606 OID 25167)
-- Name: carreras fk_rails_f4e3f0e3a9; Type: FK CONSTRAINT; Schema: public; Owner: enavarro
--

ALTER TABLE ONLY public.carreras
    ADD CONSTRAINT fk_rails_f4e3f0e3a9 FOREIGN KEY (semestre_id) REFERENCES public.semestres(id);


-- Completed on 2019-05-26 14:19:48

--
-- PostgreSQL database dump complete
--

